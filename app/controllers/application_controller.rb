class ApplicationController < ActionController::Base
  protect_from_forgery with: :exception
  before_action :set_locale
  before_action :js_routes

  def js_routes
    gon.new_dictionary_url = new_dictionary_url
  end

  private

  def set_locale
    I18n.locale = params[:locale] if params[:locale].present?
  end

  #Установить для url'ов' дабы была в них локаль
  def default_url_options(options = {})
    { locale: I18n.locale }.merge options
  end

end
