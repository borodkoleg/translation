class WordsController < ApplicationController
  before_action :authenticate_user!

  def create
    user = current_user
    image = parameters[:image]
    word = parameters[:word]
    translation_word = parameters[:translation_word]

    word_id = Word.get_id(word)
    unless word_id
      render json: { error: t("error.with_word") }
      return
    end

    if translation_word.empty?
      render json: { error: t("error.add_translation_word") }
      return
    end

    translation_word_array = translation_word.split("&")

    my_word = MyWord.find_by word_id: word_id, user_id: user.id

    if my_word
      # word already exist
      render json: { error: t("error.word_already_exist") }
      return
    else
      my_word_new = MyWord.new(word_id: word_id, user_id: user.id)
      unless my_word_new.save
        render json: { error: t("error.error_with_sistem") }
        return
      end

      my_word_id = my_word_new.id
    end

    image_id = Image.get_id(image, my_word_id)
    unless image_id
      render json: { error: t("error.image") }
      return
    end

    # addWord_have_this_word = AddWord.find_by my_word_id: my_word_id
    # if addWord_have_this_word
    #   AddWord.where(my_word_id: my_word_id).destroy_all
    # end

    if translation_word_array.size > Rails.application.secrets.translation_word_and_line.to_i
      render json: { error: t("error.too_many_word_translations") }
      return
    end

    translation_word_array.each do |t_w|
      translation_word_id = TranslationWord.get_id(t_w)
      unless translation_word_id
        render json: { error: t("error.with_translation_word") }
        return
      end

      find_already_exist = AddWord.find_by my_word_id: my_word_id,
                                           translation_word_id: translation_word_id
      unless find_already_exist
        create_addword = AddWord.new(my_word_id: my_word_id, translation_word_id: translation_word_id)

        unless create_addword.save
          render json: { error: t("error.error_with_sistem") }
          return
        end
      end
    end

    render json: { success: t("word_saved") }

  end

  def new
    gon.placeholder_translation_word = t("placeholder.translation_word")
  end

  private

  def parameters
    params.require(:add_word).permit(:word, :translation_word, :image)
  end
end
