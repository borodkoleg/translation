class DictionariesController < ApplicationController
  before_action :authenticate_user!, except: [:index]
  before_action :js_routes
  layout 'full_width_layout', only: [:index]

  def index
    if user_signed_in?
      user = current_user
      if user
        # @all = Dictionary.paginate(:page => params[:page], :per_page => 12).where user: current_user
        @all = MyWord.paginate(:page => params[:page], :per_page => 12).where user: current_user
      end
    else
     redirect_to new_user_session_url
    end
  end

  def destroy
    element = MyWord.find params[:id]

    if element.destroy
      @all = MyWord.paginate(:page => params[:page], :per_page => 12).where user: current_user
      render :partial => 'shared/index', locals: { all: @all }, data: {type: :html}
      # render "index"
      # else
      #   redirect_to root_url, notice: "Error"
    end
  end

  private

  # def parameters
  #   params.require(:dictionary).permit(:word, :line, :translation_word, :translation_line, :image)
  # end

end
