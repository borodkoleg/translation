class LinesController < ApplicationController
  before_action :authenticate_user!

  def create
    word_id = parameters[:word_id]
    line = parameters[:line]
    translation_line = parameters[:translation_line]

    if word_id.empty?
      render json: { error: t("error.add_learning_word") }, callback: "line_form_ajax"
      return
    end

    myword_find = MyWord.find_by word_id: word_id, user_id: current_user.id
    word_body = myword_find.word.body

    my_word_id = myword_find.id

    unless my_word_id
      render json: { error: t("error.error_with_sistem") }, callback: "line_form_ajax"
      return
    end

    line_array = line.split("&")
    translation_line_array = translation_line.split("&")

    if line_array.size != translation_line_array.size
      render json: { error: t("error.quantity_equal_sentence_trsentence") }, callback: "line_form_ajax"
      return
    end

    if line_array.empty?
      render json: { error: t("error.add_sentence") }, callback: "line_form_ajax"
      return
    end

    if line_array.size > Rails.application.secrets.translation_word_and_line
      render json: { error: t("error.too_many_sentence_translations") }, callback: "line_form_ajax"
      return
    end

    line_array.each_with_index do |el, index|
      unless el.include?(word_body)
        render json: { error: t("error.with_sentence") }, callback: "line_form_ajax"
        return
      end

      line_id = Line.get_id el
      unless line_id
        render json: { error: t("error.with_sentence") }, callback: "line_form_ajax"
        return
      end

      translation_line_id = TranslationLine.get_id translation_line_array[index]
      unless translation_line_id
        render json: { error: t("error.with_translation_sentence") }, callback: "line_form_ajax"
        return
      end

      find_already_exist = AddLine.find_by line_id: line_id,
                                           translation_line_id: translation_line_id,
                                           my_word_id: my_word_id
      unless find_already_exist
        new_add_line = AddLine.new(line_id: line_id,
                                   translation_line_id: translation_line_id,
                                   my_word_id: my_word_id)
        unless new_add_line.save
          render json: { error: t("error.error_with_sistem") }, callback: "line_form_ajax"
          return
        end
      end
    end

    render json: { success: t("sentence_saved") }, callback: "line_form_ajax"

  end

  def new
    @words = current_user.words
    gon.placeholder_sentence = t("placeholder.sentence")
    gon.placeholder_translation_sentence = t("placeholder.translation_sentence")
  end

  private

  def parameters
    params.require(:add_line).permit(:word_id, :line, :translation_line)
  end
end
