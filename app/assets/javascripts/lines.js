$(function() {

  window.line_form_ajax = line_form_ajax;

  function line_form_ajax(result) {
    result_message(result);

    if (result['success']) {
      var tags = $('.tags-input-line-class').tagEditor('getTags')[0].tags;
      for (i = 0; i < tags.length; i++) {
        $('.tags-input-line-class').tagEditor('removeTag', tags[i]);
      }

      var tags = $('.tags-input-line-translation-class').tagEditor('getTags')[0].tags;
      for (i = 0; i < tags.length; i++) {
        $('.tags-input-line-translation-class').tagEditor('removeTag', tags[i]);
      }
    }
  }

  //==========================

  $(".tags-input-line-class").tagEditor({
    placeholder: gon.placeholder_sentence,
    maxTags: 3,
    maxLength:100,
    delimiter: '&'
  });

  $(".tags-input-line-translation-class").tagEditor({
    placeholder: gon.placeholder_translation_sentence,
    maxTags: 3,
    maxLength:100,
    delimiter: '&'
  });

});