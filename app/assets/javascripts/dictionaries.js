$(function() {
  window.set_images = set_images;
  set_images();
// show images
  function set_images() {
    $(".custom_image").each(function (index) {

      try {

        image = $(this).attr("data-img");
        image = window.atob(image);
        $(this).attr("src", image)

      } catch (err) {

        image = "/assets/no-image.png";
        $(this).attr("src", image)
        //use empty image

      }

    });
  }

//==============================

// var player = new talkify.TtsPlayer()
//   .enableTextHighlighting();
//
// var playlist = new talkify.playlist()
//   .begin()
//   .usingPlayer(player)
//   .withRootSelector('#root')
//   .withTextInteraction()
//   .build()
//   .play();

  $('body').on("click", ".audio", function () {
    var player = new talkify.TtsPlayer();
    player.playText($(this).html());
  });


//===============================

    $('body').on("click", ".delete_button", function (e) {
      e.preventDefault();
      data_link = $(this).attr("data-link");

      Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'ice'
      }

      msg = Messenger().post({
        message: "Are you sure?",
        showCloseButton: true,
        hideAfter: 10,
        actions: {
          ok: {
            label: "Yes",
            action: function(){
              $.ajax({
                type: "POST",
                url: data_link,
                dataType: "json",
                data: {"_method":"delete"},
                complete: function(result){
                  console.log(result.responseText);
                  $("#index_content").html(result.responseText);
                  set_images();
                  $(".word_block").css("background", get_random_color);
                }
              });
              //alert(data_link);
              msg.hide()
            }
          },

          cancel: {
            label: "Cancel",
            action: function(){
              msg.hide()
            }
          }
        }
      });


    });

    $(".word_block").css("background", get_random_color);
    $(".word_and_tr_el").css("background", get_random_color);


});