// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, or any plugin's
// vendor/assets/javascripts directory can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file. JavaScript code in this file should be added after the last require_* statement.
//
// Read Sprockets README (https://github.com/rails/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery3
// require jquery_ujs
//= require rails-ujs
// require bootstrap
// require bootstrap-sprockets
//= require bootstrap
//= require drawingboard
//= require custom.js
//= require words.js
//= require lines.js
//= require dictionaries.js
//= require talkify.min.js
//= require messenger.min
//= require jquery.caret.min
//= require jquery.tag-editor.min
//= require chosen-jquery

// require jcanvas.min.js
// require_tree .
