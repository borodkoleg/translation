$(function(){
  var customBoard2 = new DrawingBoard.Board('custom-board-1', {
    controls: [
      'Color',
      {Size: {type: 'dropdown'}},
      {DrawingMode: {filler: true}},
      'Navigation'
      //'Download'
    ],
    size: 1,
    webStorage: 'session',
    enlargeYourContainer: true
  });

  window.result_message = result_message;
  function result_message(result){
    if (result['error']) {
      Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'ice'
      }

      Messenger().post({
        message:result['error'],
        showCloseButton: true,
        hideAfter: 7
      });
      //window.location = gon.new_dictionary_url;
    }else{
      $(':input',$(this).attr('class'))
        .not(':button, :submit, :reset, :hidden')
        .val('')
        .removeAttr('checked')
        .removeAttr('selected');

      /* var tags = $('.tags-input-class').tagEditor('getTags')[0].tags;
      for (i = 0; i < tags.length; i++) {
        $('.tags-input-class').tagEditor('removeTag', tags[i]);
      } */

      Messenger.options = {
        extraClasses: 'messenger-fixed messenger-on-top',
        theme: 'ice'
      }

      Messenger().post({
        message:result['success'],
        showCloseButton: true,
        hideAfter: 3
      });
    }
  }

  //=============================

  //form id
  // $('.add_line')
  //   .bind('ajax:success', function(evt, data, status, xhr){
  //     //function called on status: 200 (for ex.)
  //     console.log(data);
  //   })
  //   .bind("ajax:error", function(evt, xhr, status, error) {
  //     //function called on status: 401 or 500 (for ex.)
  //     console.log(xhr.responseText);
  //   });
  //
  // $('.add_line').bind('ajaxSuccess', function(data) {
  //   console.log(data);
  // });


  //=============================

  window.get_random_color = get_random_color
  function get_random_color() {
    var letters = '0123456789ABCDEF'.split('');
    var color = '#';
    for (var i = 0; i < 6; i++ ) {
      color += letters[Math.round(Math.random() * 15)];
    }
    return color;
  }

});