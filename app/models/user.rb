class User < ApplicationRecord
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable, :registerable,
         :rememberable, :validatable, :omniauthable, omniauth_providers: [:google_oauth2, :facebook]
  has_secure_token :rid

  has_many :my_words
  has_many :words, through: :my_words

  before_save do
    self.email = email.mb_chars.downcase.to_s
  end

  def self.from_omniauth(access_token)
    data = access_token.info
    user = User.where(email: data['email']).first

    # Uncomment the section below if you want users to be created if they don't exist
    unless user
        user = User.create(
           email: data['email'],
           password: Devise.friendly_token[0,20]
        )
    end
    user
  end

  # validates_email_format_of :email, message: 'email is not looking good'
  # validates :email, uniqueness: true, presence: true
  # validates :password, presence: true, length: { in: 5..100 }
end
