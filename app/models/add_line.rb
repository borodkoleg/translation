class AddLine < ApplicationRecord
  belongs_to :translation_line
  belongs_to :line
  belongs_to :my_word
end
