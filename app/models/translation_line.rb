class TranslationLine < ApplicationRecord
  has_many :add_lines
  has_many :my_words, through: :add_lines

  before_save do
    self.body = body.mb_chars.downcase.to_s
  end

  validates :body, uniqueness: true, presence: true, length: { in: 2..500 }

  def self.get_id translation_line
    t = TranslationLine.find_by body: translation_line
    unless t
      new_t = TranslationLine.new(body: translation_line)
      unless new_t.save
        return false
      else
        return new_t.id
      end
    else
      t.id
    end
  end

end
