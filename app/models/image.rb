class Image < ApplicationRecord
  belongs_to :my_word

  validates :body, length: { maximum: 100000 }

  def self.get_id(image, my_word_id)
    new_image = Image.new(body: image, my_word_id: my_word_id)
    unless new_image.save
      return false
    else
      return new_image.id
    end
  end
end
