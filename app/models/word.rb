class Word < ApplicationRecord

  has_many :my_words
  has_many :users, through: :my_words

  validates :body, uniqueness: true, presence: true, length: { in: 2..100 }

  before_save do
    self.body = body.mb_chars.downcase.to_s
  end

  def self.get_id(word)
    w = Word.find_by body: word
    unless w
      new_word = Word.new(body: word)
      unless new_word.save
        return false
      else
        return new_word.id
      end
    else
      w.id
    end
  end

end
