class TranslationWord < ApplicationRecord

  has_many :add_words
  has_many :my_words, through: :add_words

  before_save do
    self.body = body.mb_chars.downcase.to_s
  end

  validates :body, presence: true, length: { in: 2..100 }

  def self.get_id translation
    t = TranslationWord.find_by body: translation
    unless t
      new_t = TranslationWord.new(body: translation)
      unless new_t.save
        return false
      else
        return new_t.id
      end
    else
      t.id
    end
  end

end
