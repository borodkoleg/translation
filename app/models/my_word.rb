class MyWord < ApplicationRecord
  belongs_to :user
  belongs_to :word

  has_many :add_lines, dependent: :destroy
  has_many :lines, through: :add_lines

  has_many :add_lines, dependent: :destroy
  has_many :translation_lines, through: :add_lines

  has_many :add_words, dependent: :destroy
  has_many :translation_words, through: :add_words

  has_one :image, dependent: :destroy
end
