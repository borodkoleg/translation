class Line < ApplicationRecord
  has_many :add_lines
  has_many :my_words, through: :add_lines

  validates :body, uniqueness: true, presence: true, length: { in: 2..500 }

  before_save do
    self.body = body.mb_chars.downcase.to_s
  end

  def self.get_id line
    l = Line.find_by body: line
    unless l
      new_line = Line.new(body: line)
      unless new_line.save
        return false
      else
        return new_line.id
      end
    else
      l.id
    end
  end
end
