Rails.application.routes.draw do
  devise_for :users, only: :omniauth_callbacks, controllers: {omniauth_callbacks: 'omniauth_callbacks'}

  scope "(:locale)", locale: /en|ru/ do
    devise_for :users, skip: :omniauth_callbacks
    # For details on the DSL available within this file, see http://guides.rubyonrails.org/routing.html
    root 'dictionaries#index'
    resources :dictionaries
    resources :words
    resources :lines
    get 'study/word_in_sentence', to: 'study#word_in_sentence'
    get 'study/word_and_translate', to: 'study#word_and_translate'
    get 'study/word_and_translate', to: 'study#word_and_translate'

  end
end