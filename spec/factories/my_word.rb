FactoryGirl.define do
  factory :my_word, class: MyWord do
    user { create(:user) }
    word { create(:word) }
  end
end