FactoryGirl.define do
  factory :translation_word, class: TranslationWord do
    body { Faker::Name.name }
  end
end