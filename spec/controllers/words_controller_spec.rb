require "spec_helper"
require "rails_helper"
require "database_cleaner"
require "devise"

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean

describe WordsController do
  before do
    @user = create(:user)
    # @word = create(:word)
    # @translation_word = create(:translation_word)
    # @image = create(:image)
  end

  it "new" do
    get :new, params: { locale: 'en' }
    is_expected.to redirect_to('/users/sign_in')

    sign_in @user
    get :new, params: { locale: 'en' }
    is_expected.to render_template(:new)
  end

  it "create_without_authentication" do
    post :create, params: { add_word: { word: 'test',
                            translation_word: 'тест',
                            image: nil }}
    is_expected.to redirect_to('/users/sign_in')
  end

  it "create_word_error" do
    sign_in @user
    post :create, params: { add_word: { word: '',
                            translation_word: 'тест',
                            image: nil }}

    result = JSON.parse(response.body)
    expect(result).to include("error")
  end

  it "create_translation_word_error" do
    sign_in @user
    post :create, params: { add_word: { word: 'test',
                                        translation_word: '',
                                        image: nil }}

    result = JSON.parse(response.body)
    expect(result).to include("error")
  end

  it "words translation > 3" do
    sign_in @user
    post :create, params: { add_word: { word: 'test',
                                        translation_word: 'один&два&три&четыри',
                                        image: nil }}
    result = JSON.parse(response.body)
    expect(result).to include("error")
  end

  it "create_success" do
    sign_in @user
    post :create, params: { add_word: { word: 'test',
                                        translation_word: 'один&два',
                                        image: nil }}

    result = JSON.parse(response.body)
    expect(result).to include("success")

    myword = MyWord
    addword = AddWord
    word = Word
    image = Image
    translationword = TranslationWord

    expect(myword.all.size).to eq(1)
    expect(myword.first.user_id).to eq(@user.id)
    expect(myword.first.word_id).to eq(word.first.id)

    expect(addword.all.size).to eq(2)
    expect(addword.first.my_word_id).to eq(myword.first.id)
    expect(addword.first.translation_word_id).to eq(translationword.first.id)

    expect(word.all.size).to eq(1)
    expect(translationword.all.size).to eq(2)
    expect(image.all.size).to eq(1)
    expect(image.first.my_word_id).to eq(myword.first.id)

  end

  it "create_success_some_word" do
    sign_in @user
    post :create, params: { add_word: { word: 'test',
                                        translation_word: 'один',
                                        image: nil }}
    post :create, params: { add_word: { word: 'test',
                                        translation_word: 'один',
                                        image: nil }}

    expect(Word.all.size).to eq(1)
    expect(TranslationWord.all.size).to eq(1)
  end

  it "create_success_some_translation_word" do
    sign_in @user
    post :create, params: { add_word: { word: 'test',
                                        translation_word: 'один',
                                        image: nil }}
    post :create, params: { add_word: { word: 'test2',
                                        translation_word: 'один',
                                        image: nil }}

    expect(Word.all.size).to eq(2)
    expect(TranslationWord.all.size).to eq(1)
  end
end