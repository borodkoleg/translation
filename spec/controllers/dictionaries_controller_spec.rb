require "spec_helper"
require "rails_helper"
require "database_cleaner"
require "devise"

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean

describe DictionariesController do
  before do
    @user = create(:user)
  end

  it "index" do
    get :index
    expect(response).to redirect_to new_user_session_url
    sign_in @user

    get :index
    is_expected.to render_template(:index)

    all = MyWord.where user: @user
    expect(assigns(:all).count).to eq(all.count)
  end

  it "destroy" do
    post :destroy, params: { id: 1 }
    is_expected.to redirect_to('/users/sign_in')
  end
end

describe "DictionariesController",  :type => :request do
  before do
    post user_registration_url, params: { user: { email: '1@1.ru',
                                                  password: 'password',
                                                  password_confirmation: 'password'}}
  end

  it "destroy" do
    post user_session_url, params: { user: { email: '1@1.ru',
                                             password: 'password'}}

    post words_url, params: { add_word: { word: 'here',
                                          translation_word: 'один&два',
                                          image: nil }}

    expect(User.first.email).to eq('1@1.ru')
    expect(Word.first.body).to eq('here')

    post lines_url, params: { add_line: { word_id: Word.first.id,
                                        line: 'here some sentence1',
                                        translation_line: 'здесь некоторое предложение1' }}

    expect(Line.first.body).to eq('here some sentence1')
    expect(Image.first.body).to eq(nil)
    expect(Image.all.size).to eq(1)
    expect(TranslationWord.all.size).to eq(2)
    expect(TranslationLine.first.body).to eq('здесь некоторое предложение1')

    expect(MyWord.all.size).to eq(1)
    expect(AddLine.all.size).to eq(1)
    expect(AddWord.all.size).to eq(2)

    delete dictionary_url(MyWord.first.id)

    expect(MyWord.all.size).to eq(0)
    expect(AddLine.all.size).to eq(0)
    expect(AddWord.all.size).to eq(0)
    expect(TranslationWord.all.size).to eq(2)
    expect(TranslationLine.all.size).to eq(1)
    expect(Image.all.size).to eq(0)
    expect(Line.all.size).to eq(1)
    expect(Word.all.size).to eq(1)
  end


end