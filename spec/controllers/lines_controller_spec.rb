require "spec_helper"
require "rails_helper"
require "database_cleaner"
require "devise"

DatabaseCleaner.strategy = :truncation
DatabaseCleaner.clean

describe LinesController do
  before do
    @my_word = create(:my_word)
    @user = @my_word.user
    @word = @my_word.word
  end

  it "new" do
    get :new
    is_expected.to redirect_to('/users/sign_in')

    sign_in @user
    get :new
    is_expected.to render_template(:new)
    expect(assigns(:words)).to eq @user.words
  end

  it "create_without_authentication" do
    post :create, params: { add_line: { word_id: @word.id,
                            line: 'here some sentence&here another',
                            translation_line: 'тут какое-то предложение&тут другое' }}
    is_expected.to redirect_to('/users/sign_in')
  end

  it "create_word_id_error" do
    sign_in @user
    post :create, params: { add_line: { word_id: nil,
                                        line: 'here some sentence&here another',
                                        translation_line: 'тут какое-то предложение&тут другое' }}
    expect(response.body).to include("error")
  end

  it "create_line_error" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some sentence',
                                        translation_line: 'тут какое-то предложение&тут другое' }}
    expect(response.body).to include("error")
  end

  it "create_sentence_not_have_word_error" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'some sentence',
                                        translation_line: 'что-то еще' }}
    expect(response.body).to include("error")
  end

  it "create_translation_line_error" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some sentence',
                                        translation_line: '1' }}
    expect(response.body).to include("error")
  end

  it "line_empty_error" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: '',
                                        translation_line: '' }}
    expect(response.body).to include("error")
  end

  it "line_array.size > 3" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some&here some2&here some3&here some4',
                                        translation_line: 'что-то&что-то&что-то&что-то' }}
    expect(response.body).to include("error")
  end

  it "create_success" do
    sign_in @user
    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some sentence1',
                                        translation_line: 'здесь некоторое предложение1' }}
    expect(response.body).to include("success")

    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some sentence&some else here',
                                        translation_line: 'здесь некоторое предложение&что-то еще' }}
    expect(response.body).to include("success")

    post :create, params: { add_line: { word_id: @word.id,
                                        line: 'here some sentence&some else here',
                                        translation_line: 'здесь некоторое предложение&что-то еще' }}
    expect(response.body).to include("success")

    addLine = AddLine
    line = Line
    translationLine = TranslationLine

    expect(line.all.size).to eq(3)
    expect(translationLine.all.size).to eq(3)
    expect(addLine.all.size).to eq(3)

    expect(line.first.id).to eq(addLine.first.line_id)
    expect(translationLine.first.id).to eq(addLine.first.translation_line_id)
    expect(addLine.first.my_word_id).to eq(@my_word.id)
  end
end