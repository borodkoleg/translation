class CreateAddWords < ActiveRecord::Migration[5.1]
  def change
    create_table :my_words do |t|
      t.belongs_to :user, index: true
      t.belongs_to :word, index: true
      t.timestamps
    end
  end
end
