class CreateTranslationLines < ActiveRecord::Migration[5.1]
  def change
    create_table :translation_lines do |t|
      t.text :body
      t.timestamps
    end

  end
end
