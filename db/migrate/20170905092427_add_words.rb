class AddWords < ActiveRecord::Migration[5.1]
  def change
    create_table :add_words do |t|
      t.belongs_to :my_word, index: true
      t.belongs_to :translation_word, index: true
    end
  end
end
