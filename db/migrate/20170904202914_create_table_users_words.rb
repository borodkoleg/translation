class CreateTableUsersWords < ActiveRecord::Migration[5.1]
  def change
    create_table :add_lines do |t|
      t.belongs_to :my_word, index: true
      t.belongs_to :line, index: true
      t.belongs_to :translation_line, index: true
    end
  end
end
