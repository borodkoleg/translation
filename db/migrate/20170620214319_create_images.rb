class CreateImages < ActiveRecord::Migration[5.1]
  def change
    create_table :images do |t|
      t.text :body
      t.belongs_to :my_word
      t.timestamps
    end
  end
end
