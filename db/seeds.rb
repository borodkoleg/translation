# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)


User.delete_all
Line.delete_all
Word.delete_all
TranslationWord.delete_all
TranslationLine.delete_all
Image.delete_all
AddWord.delete_all
AddLine.delete_all
MyWord.delete_all

User.create!({:email => "220v2@mail.ru", :password => "111111", :password_confirmation => "111111" })
