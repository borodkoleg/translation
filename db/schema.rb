# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20170913140804) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "add_lines", force: :cascade do |t|
    t.bigint "my_word_id"
    t.bigint "line_id"
    t.bigint "translation_line_id"
    t.index ["line_id"], name: "index_add_lines_on_line_id"
    t.index ["my_word_id"], name: "index_add_lines_on_my_word_id"
    t.index ["translation_line_id"], name: "index_add_lines_on_translation_line_id"
  end

  create_table "add_words", force: :cascade do |t|
    t.bigint "my_word_id"
    t.bigint "translation_word_id"
    t.index ["my_word_id"], name: "index_add_words_on_my_word_id"
    t.index ["translation_word_id"], name: "index_add_words_on_translation_word_id"
  end

  create_table "images", force: :cascade do |t|
    t.text "body"
    t.bigint "my_word_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["my_word_id"], name: "index_images_on_my_word_id"
  end

  create_table "lines", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "my_words", force: :cascade do |t|
    t.bigint "user_id"
    t.bigint "word_id"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.index ["user_id"], name: "index_my_words_on_user_id"
    t.index ["word_id"], name: "index_my_words_on_word_id"
  end

  create_table "translation_lines", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "translation_words", force: :cascade do |t|
    t.text "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "users", force: :cascade do |t|
    t.string "rid", null: false
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
    t.string "email", default: "", null: false
    t.string "encrypted_password", default: "", null: false
    t.string "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer "sign_in_count", default: 0, null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet "current_sign_in_ip"
    t.inet "last_sign_in_ip"
    t.string "provider"
    t.string "uid"
    t.index ["email"], name: "index_users_on_email", unique: true
    t.index ["reset_password_token"], name: "index_users_on_reset_password_token", unique: true
  end

  create_table "words", force: :cascade do |t|
    t.string "body"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

end
